package dk.manila.customer_simulator;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.json.JSONException;
import org.json.JSONObject;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import dtu.ws.fastmoney.Account;
import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;
import dtu.ws.fastmoney.User;
import resources.CustomerResource;
import resources.RequestTokenResource;
import resources.TokenRepresentation;

/**
 * 
 * @author Alexandre, Kaloyan, Khushboo, Sebastian, Lukas, Altug
 * @author Toonw
 *
 */
public class CustomerSimulatorImpl implements CustomerSimulator {
	private final static String BASE_PATH = "http://02267-manila.compute.dtu.dk";
	private final static String CUSTOMER_MANAGER_HOSTNAME = BASE_PATH + ":8002";
	private final static String TOKEN_MANAGER_HOSTNAME = BASE_PATH + ":8004";

	private BankService bankService;

	private String cpr;
	private List<TokenRepresentation> tokens;

	@Override
	public boolean registerCustomer(String cpr, String firstName, String lastName, String amount)
			throws BankServiceException_Exception {
		this.cpr = cpr;
		retirePreviouslyCreatedBankAccount(cpr);
		createBankAccount(cpr, firstName, lastName, amount);
		
		Response responseCustomer = createCustomer(cpr);
		return responseCustomer.getStatus() == 200;

	}

	@Override
	public boolean requestTokens(int quantity) throws JsonParseException, JsonMappingException, IOException {
		RequestTokenResource tokenModel = new RequestTokenResource();
		tokenModel.quantity = 1;
		tokenModel.cprNumber = cpr;

		Client client = ClientBuilder.newClient();
		WebTarget webTarget = client.target(TOKEN_MANAGER_HOSTNAME);
		webTarget = webTarget.path("tokens");

		Response response = webTarget.request().post(Entity.entity(tokenModel, MediaType.APPLICATION_JSON));

		return (response.getStatus() == 200);
	}

	@Override
	public List<TokenRepresentation> fetchTokens() {
		Client client = ClientBuilder.newClient();
		WebTarget webTarget = client.target(CUSTOMER_MANAGER_HOSTNAME);
		webTarget = webTarget.path("customers/" + cpr + "/tokens");

		Response response = webTarget.request(MediaType.APPLICATION_JSON).get();

		try {
			JSONObject json = new JSONObject(response.readEntity(String.class));

			ObjectMapper mapper = new ObjectMapper();

			this.tokens = mapper.readValue(json.get("tokens").toString(),
					new TypeReference<List<TokenRepresentation>>() {
					});

		} catch (JSONException e) {
			e.printStackTrace();
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return tokens;
	}

	@Override
	public String getToken() {
		TokenRepresentation tmp = tokens.get(0);
		tokens.remove(0);
		return tmp.getTokenId();
	}

	@Override
	public void deregister() {
		retirePreviouslyCreatedBankAccount(cpr);

	}

	@Override
	public BigDecimal getBalance() throws BankServiceException_Exception {
		// TODO Auto-generated method stub
		bankService = new BankServiceService().getBankServicePort();
		Account account = bankService.getAccountByCprNumber(cpr);
		return account.getBalance();
	}

	private void createBankAccount(String cpr, String firstName, String lastName, String amount)
			throws BankServiceException_Exception {
		bankService = new BankServiceService().getBankServicePort();
		User bankUser = new User();
		bankUser.setCprNumber(cpr);
		bankUser.setFirstName(firstName);
		bankUser.setLastName(lastName);
		BigDecimal balance = new BigDecimal(amount);
		bankService.createAccountWithBalance(bankUser, balance);
	}

	private void retirePreviouslyCreatedBankAccount(String cpr) {
		bankService = new BankServiceService().getBankServicePort();
		try {
			Account account = bankService.getAccountByCprNumber(cpr);
			bankService.retireAccount(account.getId());
		} catch (BankServiceException_Exception e) {
		}
	}

	private Response createCustomer(String cprNumber) {
		CustomerResource customerResource = new CustomerResource();
		customerResource.cprNumber = cprNumber;
		Client client = ClientBuilder.newClient();
		WebTarget webTarget = client.target(CUSTOMER_MANAGER_HOSTNAME).path("customers");
		return webTarget.request().post(Entity.entity(customerResource, MediaType.APPLICATION_JSON));
	}

}
