package dk.manila.customer_simulator;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import dtu.ws.fastmoney.BankServiceException_Exception;
import resources.TokenRepresentation;

/**
 * Class for simulating Customer
 *
 * @author Alexandre, Kaloyan, Khushboo, Sebastian, Lukas, Altug
 */
public interface CustomerSimulator {
	/**
	 * Registers Customer into the DTUPay system.
	 * 
	 * @param cpr
     *     allowed object is
     *     {@link String }
	 * @param firstName
     *     allowed object is
     *     {@link String }
	 * @param lastName
     *     allowed object is
     *     {@link String }
	 * @param amount
     *     allowed object is
     *     {@link String }
	 * @return
     *     possible object is
     *     {@link boolean }
	 * @throws BankServiceException_Exception
	 */
	boolean registerCustomer(String cpr, String firstName, String lastName, String amount) throws BankServiceException_Exception;
	/**
	 * Requests tokens from Token Manager.
	 * 
	 * @param quantity
     *     allowed object is
     *     {@link int }
	 * @return
     *     possible object is
     *     {@link boolean }
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	boolean requestTokens(int quantity) throws JsonParseException, JsonMappingException, IOException;
	
	/**
	 * Fetches the tokens that customer have from CustomerManager
	 * 
	 * @return
     *     possible object is
     *     {@link List<TokenRepresentation> }
	 */
	List<TokenRepresentation> fetchTokens();
	/**
	 * Gets one token from customer's tokens.
	 * 
	 * @return
     *     possible object is
     *     {@link String }
	 */
	String getToken();
	/**
	 * Deletes the customer from DTUPay system.
	 */
	void deregister();
	
	
	/**
	 * Gets the bank account balance of the customer.
	 * 
	 * @return
     *     possible object is
     *     {@link BigDecimal }
	 * @throws BankServiceException_Exception
	 */
	BigDecimal getBalance() throws BankServiceException_Exception;

}
