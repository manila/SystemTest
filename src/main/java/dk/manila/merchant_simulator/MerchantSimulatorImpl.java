package dk.manila.merchant_simulator;

import java.math.BigDecimal;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import dtu.ws.fastmoney.Account;
import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;
import dtu.ws.fastmoney.User;
import resources.MerchantResource;
import resources.PaymentResource;

/**
 * 
 * @author Alexandre, Kaloyan, Khushboo, Sebastian, Lukas, Altug
 * @author Toonw
 *
 */
public class MerchantSimulatorImpl implements MerchantSimulator {
	private final static String BASE_PATH = "http://02267-manila.compute.dtu.dk";
	private final static String PAYMENT_MANAGER_HOSTNAME = BASE_PATH + ":8001";
	private final static String MERCHANT_MANAGER_HOSTNAME = BASE_PATH + ":8003";

	private String cpr;
	private BankService bankService;
	private String token;
	private Response paymentResponse;

	@Override
	public boolean registerMerchant(String cpr, String firstName, String lastName, String amount)
			throws BankServiceException_Exception {
		this.cpr = cpr;
		retirePreviouslyCreatedBankAccount(cpr);
		createBankAccount(cpr, firstName, lastName, amount);
		Response reponse = createMerchant(this.cpr);
		return reponse.getStatus() == 200;
	}

	@Override
	public void scan(String token) {
		this.token = token;
	}

	@Override
	public String pay(int amount) {
		Client client = ClientBuilder.newClient();
		WebTarget webTarget = client.target(PAYMENT_MANAGER_HOSTNAME);
		webTarget = webTarget.path("payments");

		PaymentResource paymentRequest = new PaymentResource();
		paymentRequest.payerTokenId = this.token;
		paymentRequest.payeeCpr = this.cpr;
		paymentRequest.amount = String.valueOf(amount);

		paymentResponse = webTarget.request().post(Entity.entity(paymentRequest, MediaType.APPLICATION_JSON));

		String errMessage = paymentResponse.readEntity(String.class);

		return errMessage;
	}

	@Override
	public void deregister() {
		retirePreviouslyCreatedBankAccount(cpr);
	}

	@Override
	public BigDecimal getBalance() throws BankServiceException_Exception {
		bankService = new BankServiceService().getBankServicePort();
		Account account = bankService.getAccountByCprNumber(cpr);
		return account.getBalance();
	}

	private void createBankAccount(String cpr, String firstName, String lastName, String amount)
			throws BankServiceException_Exception {
		bankService = new BankServiceService().getBankServicePort();
		User bankUser = new User();
		bankUser.setCprNumber(cpr);
		bankUser.setFirstName(firstName);
		bankUser.setLastName(lastName);
		BigDecimal balance = new BigDecimal(amount);
		bankService.createAccountWithBalance(bankUser, balance);
	}

	private void retirePreviouslyCreatedBankAccount(String cpr) {
		bankService = new BankServiceService().getBankServicePort();

		try {
			Account account = bankService.getAccountByCprNumber(cpr);
			bankService.retireAccount(account.getId());
		} catch (BankServiceException_Exception e) {
		}
	}

	private Response createMerchant(String cprNumber) {
		MerchantResource merchantResource = new MerchantResource();
		merchantResource.cprNumber = cprNumber;
		Client client = ClientBuilder.newClient();
		WebTarget webTarget = client.target(MERCHANT_MANAGER_HOSTNAME).path("merchants");
		return webTarget.request().post(Entity.entity(merchantResource, MediaType.APPLICATION_JSON));
	}

}
