package dk.manila.merchant_simulator;

import java.math.BigDecimal;

import dtu.ws.fastmoney.BankServiceException_Exception;

/**
 * 
 * Class for simulating Merchant
 *
 * @author Alexandre, Kaloyan, Khushboo, Sebastian, Lukas, Altug
 */
public interface MerchantSimulator {
	/**
	 * 
	 * Registers Merchant into the DTUPay system.
	 * 
	 * @param cpr
	 *            allowed object is {@link String }
	 * @param firstName
	 *            allowed object is {@link String }
	 * @param lastName
	 *            allowed object is {@link String }
	 * @param amount
	 *            allowed object is {@link String }
	 * @return possible object is {@link boolean }
	 * @throws BankServiceException_Exception
	 */
	boolean registerMerchant(String cpr, String firstName, String lastName, String amount)
			throws BankServiceException_Exception;

	/**
	 * Scans the token that is provided by customer.
	 * 
	 * @param token
	 *            allowed object is {@link String }
	 */
	void scan(String token);

	/**
	 * Function to make the payment operation.
	 * 
	 * @param amount
	 *            allowed object is {@link int }
	 * @return possible object is {@link boolean }
	 */
	String pay(int amount);

	/**
	 * Deletes the merchant from DTUPay system.
	 */
	void deregister();

	/**
	 * Gets the bank account balance of the merchant.
	 * 
	 * @return possible object is {@link BigDecimal }
	 * @throws BankServiceException_Exception
	 */
	BigDecimal getBalance() throws BankServiceException_Exception;

}
