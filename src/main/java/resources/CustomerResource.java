package resources;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
/**
 * 
 * @author Alexandre, Kaloyan, Khushboo, Sebastian, Lukas, Altug
 * @author Toonw
 *
 */
@XmlRootElement()
public class CustomerResource {
	@XmlElement public String cprNumber;
}
