package resources;

import java.io.IOException;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.commons.io.output.ByteArrayOutputStream;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.oned.Code128Writer;

/**
 * 
 * @author lukas
 * Representation of a Token
 * This class is used when a token needs to be presented for a customer
 *
 */

@XmlRootElement
public class TokenRepresentation {

	@XmlElement private String tokenId;
	@XmlElement private String barcodeUri;
	
	public String getTokenId() {
		return tokenId;
	}

	public String getBarcodeUri() {
		return barcodeUri;
	}
	
	/**
	 * 
	 * @return A byte array containing the image of a barcode representing the tokenId
	 * @throws WriterException
	 * @throws IOException
	 */
	public byte[] generateBarcodeImage() throws WriterException, IOException {
		int width = 250;
		int height = 150;
		
		BitMatrix barcode = new Code128Writer().encode(this.tokenId.toString(), BarcodeFormat.CODE_128, width, height);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		MatrixToImageWriter.writeToStream(barcode, "png", out);
		
		return (byte[]) out.toByteArray();
	}
	
}
