package resources;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Alexandre, Kaloyan, Khushboo, Sebastian, Lukas, Altug
 *
 */
@XmlRootElement
public class RequestTokenResource {

	@XmlElement public String cprNumber;
	@XmlElement public int quantity;
	
}
