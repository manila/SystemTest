package resources;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * 
 * @author Alexandre, Kaloyan, Khushboo, Sebastian, Lukas, Altug
 *
 */
@XmlRootElement
public class PaymentResource {
	@XmlElement public String payerTokenId;
	@XmlElement public String payeeCpr;
	@XmlElement public String amount;
}
