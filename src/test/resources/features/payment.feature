## @author Alexandre, Kaloyan, Khushboo, Sebastian, Lukas, Altug
Feature: Payment
        
	Scenario: Simple payment
		Given A registered customer with cpr "8102490211" with a bank account
		And A registered merchant with cpr "3649263872" with a bank account
		And The customer has one unused token
		When The merchant scans the token
		And Requests payment for 100 kr using the token
		Then The payment succeeds
		And The money is transferred from the customer bank account to the merchant bank account   
          
	Scenario: Paying with a token makes it used
		Given A registered customer with cpr "3427837621" with a bank account
		And A registered merchant with cpr "3028465827" with a bank account
		And The customer has one unused token
		When The merchant scans the token
		And Requests payment for 100 kr using the token
		Then The payment succeeds
		And The customer has no unused tokens left

	Scenario: Paying with a token twice
	 	Given A registered customer with cpr "8392642719" with a bank account
		And A registered merchant with cpr "0298452738" with a bank account
		And The customer has one unused token
		And The customer copies the token
		When The merchant scans the original token
		And Requests payment for 100 kr using the original token
		And The merchant scans the token copy
		And Requests payment for 100 kr using the token copy
		Then The payment succeeds for the original token
		And The payment is declined for the token copy