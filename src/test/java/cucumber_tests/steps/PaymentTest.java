package cucumber_tests.steps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.List;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dk.manila.customer_simulator.CustomerSimulator;
import dk.manila.customer_simulator.CustomerSimulatorImpl;
import dk.manila.merchant_simulator.MerchantSimulator;
import dk.manila.merchant_simulator.MerchantSimulatorImpl;
import resources.TokenRepresentation;

public class PaymentTest {

	private String cprCustomer;
	private String cprMerchant;


	private TokenRepresentation tokenRepresentation;
	private TokenRepresentation tokenRepresentationCopy;
	private String returnMessage;
	private String usedTokenMessage;
	private BigDecimal amount;


	private final CustomerSimulator customerSimulator = new CustomerSimulatorImpl();
	private final MerchantSimulator merchantSimulator = new MerchantSimulatorImpl();

	@Given("^A registered customer with cpr \"([^\"]*)\" with a bank account$")
	public void a_registered_customer_with_cpr_with_a_bank_account(String cpr) throws Exception {
		this.cprCustomer = cpr;
		assertTrue(customerSimulator.registerCustomer(this.cprCustomer, "Bob", "Marley", "200"));

	}

	@Given("^A registered merchant with cpr \"([^\"]*)\" with a bank account$")
	public void a_registered_merchant_with_cpr_with_a_bank_account(String cpr) throws Exception {
		this.cprMerchant = cpr;
		assertTrue(merchantSimulator.registerMerchant(this.cprMerchant, "Bill", "Gates", "0"));
	}

	@Given("^The customer has one unused token$")
	public void the_customer_has_one_unused_token() throws Exception {
		assertTrue(customerSimulator.requestTokens(1));
		List<TokenRepresentation> tokens = customerSimulator.fetchTokens();

		assertEquals(1, tokens.size());

		tokenRepresentation = tokens.get(0);
	}

	@When("^The merchant scans the token$")
	public void the_merchant_scans_the_token() throws Exception {
		merchantSimulator.scan(tokenRepresentation.getTokenId());
	}

	@When("^Requests payment for (\\d+) kr using the token$")
	public void requests_payment_for_kr_using_the_token(int amount) throws Exception {
		this.amount = new BigDecimal(amount);
		this.returnMessage = merchantSimulator.pay(amount);
	}

	@Then("^The payment succeeds$")
	public void the_payment_succeeds() throws Exception {

		assertEquals("Payment successful", this.returnMessage);
	}

	@Then("^The money is transferred from the customer bank account to the merchant bank account$")
	public void the_money_is_transferred_from_the_customer_bank_account_to_the_merchant_bank_account()
			throws Exception {
		assertEquals(this.amount, customerSimulator.getBalance());
		assertEquals(this.amount, merchantSimulator.getBalance());

		customerSimulator.deregister();
		merchantSimulator.deregister();
	}

	@Then("^The customer has no unused tokens left$")
	public void the_customer_has_no_unused_tokens_left() throws Exception {

		List<TokenRepresentation> tokens = customerSimulator.fetchTokens();

		assertEquals(0, tokens.size());
		
		customerSimulator.deregister();
		merchantSimulator.deregister();
	}

	@Given("^The customer copies the token$")
	public void the_customer_copies_the_token() throws Exception {
		this.tokenRepresentationCopy = this.tokenRepresentation;
	}

	@When("^The merchant scans the original token$")
	public void the_merchant_scans_the_original_token() throws Exception {
		
		merchantSimulator.scan(tokenRepresentation.getTokenId());
	}

	@When("^The merchant scans the token copy$")
	public void the_merchant_scans_the_token_copy() throws Exception {

		merchantSimulator.scan(tokenRepresentationCopy.getTokenId());
	}

	@When("^Requests payment for (\\d+) kr using the original token$")
	public void requests_payment_for_kr_using_the_original_token(int amount) throws Exception {

		String returnMessage = merchantSimulator.pay(amount);

		assertEquals("Payment successful", returnMessage);

	}

	@When("^Requests payment for (\\d+) kr using the token copy$")
	public void requests_payment_for_kr_using_the_token_copy(int amount) throws Exception {
		this.amount = new BigDecimal(amount);
		this.usedTokenMessage = merchantSimulator.pay(amount);

	}

	@Then("^The payment succeeds for the original token$")
	public void the_payment_succeeds_for_the_original_token() throws Exception {
		assertEquals(this.amount, customerSimulator.getBalance());
		assertEquals(this.amount, merchantSimulator.getBalance());
	}

	@Then("^The payment is declined for the token copy$")
	public void the_payment_is_declined_for_the_token_copy() throws Exception {

		assertEquals("Token is already used", this.usedTokenMessage);
		customerSimulator.deregister();
		merchantSimulator.deregister();
	}

}
